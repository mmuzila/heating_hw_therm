EESchema Schematic File Version 4
LIBS:therm-cache
EELAYER 26 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "Bezdrôtový teplotný senzor"
Date "2019-05-15"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L misc:RFM69HW U3
U 1 1 5CAB7C44
P 6450 3700
F 0 "U3" H 6450 4200 55  0000 C CNN
F 1 "RFM69HW" H 6450 4300 55  0000 C CNN
F 2 "misc:RFM69HW" H 6450 3700 55  0001 C CNN
F 3 "" H 6450 3700 55  0001 C CNN
	1    6450 3700
	-1   0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATmega:ATmega8-16PU U1
U 1 1 5CAB7DA3
P 2500 4350
F 0 "U1" H 1850 3100 50  0000 C CNN
F 1 "ATmega8-16PU" H 2050 2950 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W7.62mm_Socket" H 2500 4350 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2486-8-bit-avr-microcontroller-atmega8_l_datasheet.pdf" H 2500 4350 50  0001 C CNN
	1    2500 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 5CAB81A0
P 5850 4100
F 0 "#PWR012" H 5850 3850 50  0001 C CNN
F 1 "GND" H 5855 3927 50  0000 C CNN
F 2 "" H 5850 4100 50  0001 C CNN
F 3 "" H 5850 4100 50  0001 C CNN
	1    5850 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 3450 7150 3450
Wire Wire Line
	6950 3350 6950 3050
Wire Wire Line
	3450 3450 3450 3350
Wire Wire Line
	3450 3350 3100 3350
Wire Wire Line
	3100 3250 3250 3250
Wire Wire Line
	3250 3250 3250 3050
$Comp
L power:+3.3V #PWR014
U 1 1 5CABA23D
P 7400 3200
F 0 "#PWR014" H 7400 3050 50  0001 C CNN
F 1 "+3.3V" H 7415 3373 50  0000 C CNN
F 2 "" H 7400 3200 50  0001 C CNN
F 3 "" H 7400 3200 50  0001 C CNN
	1    7400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 3200 7400 4050
Wire Wire Line
	7400 4050 6950 4050
$Comp
L Device:R R1
U 1 1 5CABDA4A
P 1700 3050
F 0 "R1" H 1770 3096 50  0000 L CNN
F 1 "inf" H 1770 3005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 1630 3050 50  0001 C CNN
F 3 "~" H 1700 3050 50  0001 C CNN
	1    1700 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 3250 1700 3250
Wire Wire Line
	1700 3250 1700 3200
$Comp
L power:+3V3 #PWR05
U 1 1 5CABE5B7
P 1700 2750
F 0 "#PWR05" H 1700 2600 50  0001 C CNN
F 1 "+3V3" H 1715 2923 50  0000 C CNN
F 2 "" H 1700 2750 50  0001 C CNN
F 3 "" H 1700 2750 50  0001 C CNN
	1    1700 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2900 1700 2750
$Comp
L Switch:SW_Push SW1
U 1 1 5CABF173
P 1300 3250
F 0 "SW1" H 1300 3535 50  0000 C CNN
F 1 "SW_Push" H 1300 3444 50  0000 C CNN
F 2 "misc:P-DT6SW" H 1300 3450 50  0001 C CNN
F 3 "" H 1300 3450 50  0001 C CNN
	1    1300 3250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5CABF2E0
P 1000 3350
F 0 "#PWR03" H 1000 3100 50  0001 C CNN
F 1 "GND" H 1005 3177 50  0000 C CNN
F 2 "" H 1000 3350 50  0001 C CNN
F 3 "" H 1000 3350 50  0001 C CNN
	1    1000 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3250 1000 3250
Wire Wire Line
	1000 3250 1000 3350
Wire Wire Line
	1500 3250 1550 3250
Connection ~ 1700 3250
$Comp
L Switch:SW_Push SW2
U 1 1 5CAC0B0A
P 4300 5400
F 0 "SW2" H 4300 5685 50  0000 C CNN
F 1 "SW_Push" H 4300 5594 50  0000 C CNN
F 2 "misc:P-DT6SW" H 4300 5600 50  0001 C CNN
F 3 "" H 4300 5600 50  0001 C CNN
	1    4300 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 5400 3900 5400
$Comp
L power:+3V3 #PWR010
U 1 1 5CAC1B55
P 4650 5300
F 0 "#PWR010" H 4650 5150 50  0001 C CNN
F 1 "+3V3" H 4665 5473 50  0000 C CNN
F 2 "" H 4650 5300 50  0001 C CNN
F 3 "" H 4650 5300 50  0001 C CNN
	1    4650 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5400 4650 5400
Wire Wire Line
	4650 5400 4650 5300
$Comp
L power:GND #PWR06
U 1 1 5CAC2AAC
P 2500 5900
F 0 "#PWR06" H 2500 5650 50  0001 C CNN
F 1 "GND" H 2505 5727 50  0000 C CNN
F 2 "" H 2500 5900 50  0001 C CNN
F 3 "" H 2500 5900 50  0001 C CNN
	1    2500 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 5750 2500 5800
Wire Wire Line
	2500 5800 2600 5800
Wire Wire Line
	2600 5800 2600 5750
Connection ~ 2500 5800
Wire Wire Line
	2500 5800 2500 5900
$Comp
L Sensor_Temperature:DS18B20 U2
U 1 1 5CAC4B90
P 3900 4300
F 0 "U2" H 3671 4346 50  0000 R CNN
F 1 "DS18B20" H 3671 4255 50  0000 R CNN
F 2 "Package_TO_SOT_THT:TO-92L_Inline_Wide" H 2900 4050 50  0001 C CNN
F 3 "http://datasheets.maximintegrated.com/en/ds/DS18B20.pdf" H 3750 4550 50  0001 C CNN
	1    3900 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7150 3450 7150 4850
Wire Wire Line
	3100 4950 3900 4950
Wire Wire Line
	3900 4950 3900 5400
$Comp
L Device:R R4
U 1 1 5CAC7F8C
P 3900 5600
F 0 "R4" H 3970 5646 50  0000 L CNN
F 1 "10K" H 3970 5555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 3830 5600 50  0001 C CNN
F 3 "~" H 3900 5600 50  0001 C CNN
	1    3900 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 5450 3900 5400
Connection ~ 3900 5400
$Comp
L power:GND #PWR09
U 1 1 5CAC9168
P 3900 5850
F 0 "#PWR09" H 3900 5600 50  0001 C CNN
F 1 "GND" H 3905 5677 50  0000 C CNN
F 2 "" H 3900 5850 50  0001 C CNN
F 3 "" H 3900 5850 50  0001 C CNN
	1    3900 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 5750 3900 5800
$Comp
L Device:R R2
U 1 1 5CACC99F
P 3350 4050
F 0 "R2" V 3143 4050 50  0000 C CNN
F 1 "inf" V 3234 4050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 3280 4050 50  0001 C CNN
F 3 "~" H 3350 4050 50  0001 C CNN
	1    3350 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 4050 3200 4050
Wire Wire Line
	3100 3950 3900 3950
Wire Wire Line
	3100 4150 3500 4150
Wire Wire Line
	3500 4050 3500 4150
$Comp
L power:GND #PWR08
U 1 1 5CAD2156
P 3900 4650
F 0 "#PWR08" H 3900 4400 50  0001 C CNN
F 1 "GND" H 4050 4550 50  0000 C CNN
F 2 "" H 3900 4650 50  0001 C CNN
F 3 "" H 3900 4650 50  0001 C CNN
	1    3900 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4650 3900 4600
Wire Wire Line
	3900 4000 3900 3950
Wire Wire Line
	3500 4150 3500 4300
Wire Wire Line
	3500 4300 3600 4300
Connection ~ 3500 4150
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5CAD652B
P 3550 2800
F 0 "J3" H 3470 2475 50  0000 C CNN
F 1 "Conn_01x02" H 3470 2566 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3550 2800 50  0001 C CNN
F 3 "~" H 3550 2800 50  0001 C CNN
	1    3550 2800
	-1   0    0    1   
$EndComp
Wire Wire Line
	3900 3950 3900 2800
Wire Wire Line
	3900 2800 3750 2800
Connection ~ 3900 3950
$Comp
L power:+3V3 #PWR07
U 1 1 5CAD7E6F
P 3750 2300
F 0 "#PWR07" H 3750 2150 50  0001 C CNN
F 1 "+3V3" H 3765 2473 50  0000 C CNN
F 2 "" H 3750 2300 50  0001 C CNN
F 3 "" H 3750 2300 50  0001 C CNN
	1    3750 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2700 3750 2300
$Comp
L Device:LED D1
U 1 1 5CAD98C2
P 3600 5250
F 0 "D1" V 3638 5133 50  0000 R CNN
F 1 "LED" V 3547 5133 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm" H 3600 5250 50  0001 C CNN
F 3 "~" H 3600 5250 50  0001 C CNN
	1    3600 5250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 5CAD9AA2
P 3600 5600
F 0 "R3" H 3670 5646 50  0000 L CNN
F 1 "270" H 3670 5555 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 3530 5600 50  0001 C CNN
F 3 "~" H 3600 5600 50  0001 C CNN
	1    3600 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 5050 3600 5050
Wire Wire Line
	3600 5450 3600 5400
Wire Wire Line
	3600 5100 3600 5050
Wire Wire Line
	3600 5750 3600 5800
Wire Wire Line
	3600 5800 3900 5800
Connection ~ 3900 5800
Wire Wire Line
	3900 5800 3900 5850
$Comp
L Device:Crystal Y1
U 1 1 5CAE0867
P 1450 3950
F 0 "Y1" H 1450 3682 50  0000 C CNN
F 1 "32786Hz" H 1450 3773 50  0000 C CNN
F 2 "Crystal:Crystal_AT310_D3.0mm_L10.0mm_Vertical" H 1450 3950 50  0001 C CNN
F 3 "~" H 1450 3950 50  0001 C CNN
	1    1450 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	1600 3950 1650 3950
Wire Wire Line
	1650 3950 1650 3650
Wire Wire Line
	1650 3650 1900 3650
Wire Wire Line
	1300 3950 1250 3950
Wire Wire Line
	1250 3950 1250 3450
Wire Wire Line
	1250 3450 1900 3450
$Comp
L Device:C C1
U 1 1 5CAE4C64
P 1250 4150
F 0 "C1" H 1365 4196 50  0000 L CNN
F 1 "24p" H 1365 4105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 1288 4000 50  0001 C CNN
F 3 "~" H 1250 4150 50  0001 C CNN
	1    1250 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5CAE4CEF
P 1650 4150
F 0 "C2" H 1765 4196 50  0000 L CNN
F 1 "24p" H 1765 4105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.1mm_W3.2mm_P5.00mm" H 1688 4000 50  0001 C CNN
F 3 "~" H 1650 4150 50  0001 C CNN
	1    1650 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 4000 1250 3950
Connection ~ 1250 3950
Wire Wire Line
	1650 4000 1650 3950
Connection ~ 1650 3950
$Comp
L power:GND #PWR04
U 1 1 5CAE95FC
P 1250 4400
F 0 "#PWR04" H 1250 4150 50  0001 C CNN
F 1 "GND" H 1255 4227 50  0000 C CNN
F 2 "" H 1250 4400 50  0001 C CNN
F 3 "" H 1250 4400 50  0001 C CNN
	1    1250 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 4300 1250 4350
Wire Wire Line
	1250 4350 1650 4350
Wire Wire Line
	1650 4350 1650 4300
Connection ~ 1250 4350
Wire Wire Line
	1250 4350 1250 4400
$Comp
L Connector:AVR-ISP-10 J5
U 1 1 5CAEE65D
P 5300 1800
F 0 "J5" V 4883 1850 50  0000 C CNN
F 1 "AVR-ISP-10" V 4974 1850 50  0000 C CNN
F 2 "Connector_IDC:IDC-Header_2x05_P2.54mm_Vertical" V 5050 1850 50  0001 C CNN
F 3 " ~" H 4025 1250 50  0001 C CNN
	1    5300 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 3050 6950 3050
Wire Wire Line
	5950 3350 5850 3350
Wire Wire Line
	5850 3350 5850 3850
Connection ~ 5850 3850
Wire Wire Line
	5850 3850 5950 3850
Wire Wire Line
	3100 4850 7150 4850
Text GLabel 5200 2300 3    50   Input ~ 0
RST
Text GLabel 1550 2850 1    50   Input ~ 0
RST
Wire Wire Line
	1550 2850 1550 3250
Connection ~ 1550 3250
Wire Wire Line
	1550 3250 1700 3250
$Comp
L Device:R R5
U 1 1 5CB10CCE
P 5750 2800
F 0 "R5" H 5820 2846 50  0000 L CNN
F 1 "10k" H 5820 2755 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P15.24mm_Horizontal" V 5680 2800 50  0001 C CNN
F 3 "~" H 5750 2800 50  0001 C CNN
	1    5750 2800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR013
U 1 1 5CB13697
P 5750 2500
F 0 "#PWR013" H 5750 2350 50  0001 C CNN
F 1 "+3V3" H 5765 2673 50  0000 C CNN
F 2 "" H 5750 2500 50  0001 C CNN
F 3 "" H 5750 2500 50  0001 C CNN
	1    5750 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 2300 5200 2200
Wire Wire Line
	5750 2650 5750 2500
Wire Wire Line
	5750 2950 5750 3450
Connection ~ 5750 3450
Wire Wire Line
	5750 3450 5950 3450
Wire Wire Line
	5500 2200 5500 3650
Connection ~ 5500 3650
Wire Wire Line
	5500 3650 5950 3650
Wire Wire Line
	5400 2200 5400 3550
Connection ~ 5400 3550
Wire Wire Line
	5400 3550 5950 3550
Wire Wire Line
	5300 2200 5300 3750
Connection ~ 5300 3750
Wire Wire Line
	5300 3750 5950 3750
$Comp
L power:GND #PWR011
U 1 1 5CB2BF66
P 4850 1800
F 0 "#PWR011" H 4850 1550 50  0001 C CNN
F 1 "GND" H 4855 1627 50  0000 C CNN
F 2 "" H 4850 1800 50  0001 C CNN
F 3 "" H 4850 1800 50  0001 C CNN
	1    4850 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1700 4850 1700
Wire Wire Line
	4850 1700 4850 1800
$Comp
L power:GND #PWR02
U 1 1 5CB2F3E7
P 6350 6400
F 0 "#PWR02" H 6350 6150 50  0001 C CNN
F 1 "GND" H 6355 6227 50  0000 C CNN
F 2 "" H 6350 6400 50  0001 C CNN
F 3 "" H 6350 6400 50  0001 C CNN
	1    6350 6400
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0101
U 1 1 5CB367B5
P 2500 2450
F 0 "#PWR0101" H 2500 2300 50  0001 C CNN
F 1 "+3V3" H 2515 2623 50  0000 C CNN
F 2 "" H 2500 2450 50  0001 C CNN
F 3 "" H 2500 2450 50  0001 C CNN
	1    2500 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 2950 2500 2750
Wire Wire Line
	2500 2750 2600 2750
Wire Wire Line
	2600 2750 2600 2950
Connection ~ 2500 2750
Wire Wire Line
	2500 2750 2500 2450
Wire Wire Line
	3450 3450 5750 3450
Wire Wire Line
	3100 3550 5400 3550
Wire Wire Line
	3100 3650 5500 3650
Wire Wire Line
	3100 3750 5300 3750
Wire Wire Line
	5850 3850 5850 4050
Wire Wire Line
	5950 4050 5850 4050
Connection ~ 5850 4050
Wire Wire Line
	5850 4050 5850 4100
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 5CB43C7F
P 5300 4050
F 0 "J4" H 5300 4150 50  0000 C CNN
F 1 "Conn_01x02" H 5300 4250 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5300 4050 50  0001 C CNN
F 3 "~" H 5300 4050 50  0001 C CNN
	1    5300 4050
	-1   0    0    1   
$EndComp
Wire Wire Line
	5950 3950 5500 3950
Wire Wire Line
	5500 4050 5850 4050
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5CB496AA
P 3750 6300
F 0 "J2" H 3900 6150 50  0000 C CNN
F 1 "Conn_01x03" H 4050 6250 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3750 6300 50  0001 C CNN
F 3 "~" H 3750 6300 50  0001 C CNN
	1    3750 6300
	1    0    0    1   
$EndComp
Wire Wire Line
	3550 6200 3500 6200
Wire Wire Line
	3500 6200 3500 4650
Wire Wire Line
	3500 4650 3100 4650
Wire Wire Line
	3100 4750 3400 4750
Wire Wire Line
	3400 4750 3400 6300
Wire Wire Line
	3400 6300 3550 6300
$Comp
L power:GND #PWR015
U 1 1 5CB551DE
P 3500 6500
F 0 "#PWR015" H 3500 6250 50  0001 C CNN
F 1 "GND" H 3505 6327 50  0000 C CNN
F 2 "" H 3500 6500 50  0001 C CNN
F 3 "" H 3500 6500 50  0001 C CNN
	1    3500 6500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6500 3500 6400
Wire Wire Line
	3500 6400 3550 6400
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5CB58A35
P 6900 5800
F 0 "J1" H 6980 5792 50  0000 L CNN
F 1 "Conn_01x02" H 6980 5701 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6900 5800 50  0001 C CNN
F 3 "~" H 6900 5800 50  0001 C CNN
	1    6900 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery_Cell BT1
U 1 1 5CB5B89B
P 6350 6150
F 0 "BT1" H 6468 6246 50  0000 L CNN
F 1 "LS14500CNA" H 6468 6155 50  0000 L CNN
F 2 "misc:Baterry-LS14500CNA" V 6350 6210 50  0001 C CNN
F 3 "~" V 6350 6210 50  0001 C CNN
	1    6350 6150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 6400 6350 6250
Wire Wire Line
	6350 5900 6350 5950
Wire Wire Line
	6350 5900 6700 5900
$Comp
L power:+3V3 #PWR01
U 1 1 5CB60D95
P 6350 5650
F 0 "#PWR01" H 6350 5500 50  0001 C CNN
F 1 "+3V3" H 6365 5823 50  0000 C CNN
F 2 "" H 6350 5650 50  0001 C CNN
F 3 "" H 6350 5650 50  0001 C CNN
	1    6350 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 5800 6350 5800
Wire Wire Line
	6350 5800 6350 5650
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 5CAC0FE0
P 1900 7200
F 0 "H1" H 2000 7251 50  0000 L CNN
F 1 "hole" H 2000 7160 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1900 7200 50  0001 C CNN
F 3 "~" H 1900 7200 50  0001 C CNN
	1    1900 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 5CAC10FE
P 2300 7200
F 0 "H2" H 2400 7251 50  0000 L CNN
F 1 "hole" H 2400 7160 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 2300 7200 50  0001 C CNN
F 3 "~" H 2300 7200 50  0001 C CNN
	1    2300 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H3
U 1 1 5CAC114E
P 2650 7200
F 0 "H3" H 2750 7251 50  0000 L CNN
F 1 "hole" H 2750 7160 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 2650 7200 50  0001 C CNN
F 3 "~" H 2650 7200 50  0001 C CNN
	1    2650 7200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H4
U 1 1 5CAC1196
P 2950 7200
F 0 "H4" H 3050 7251 50  0000 L CNN
F 1 "hole" H 3050 7160 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 2950 7200 50  0001 C CNN
F 3 "~" H 2950 7200 50  0001 C CNN
	1    2950 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 7300 1900 7400
Wire Wire Line
	1900 7400 2300 7400
Wire Wire Line
	2300 7400 2300 7300
Wire Wire Line
	2300 7400 2650 7400
Wire Wire Line
	2650 7400 2650 7300
Connection ~ 2300 7400
Wire Wire Line
	2650 7400 2950 7400
Wire Wire Line
	2950 7400 2950 7300
Connection ~ 2650 7400
$Comp
L power:GND #PWR0102
U 1 1 5CACA704
P 2300 7600
F 0 "#PWR0102" H 2300 7350 50  0001 C CNN
F 1 "GND" H 2305 7427 50  0000 C CNN
F 2 "" H 2300 7600 50  0001 C CNN
F 3 "" H 2300 7600 50  0001 C CNN
	1    2300 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 7400 2300 7600
$EndSCHEMATC
